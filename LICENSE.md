# Licence

Copyright 2014-2021 Association Sésamath

## English

Sesatheque is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

Sesatheque is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Sesatheque (LICENSE file).

More on http://www.gnu.org/licenses/agpl.txt
and https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)

## Français

Sésathèque est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant
les termes de la GNU Affero General Public License version 3 telle que publiée par la
Free Software Foundation.

Sésathèque est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE,
sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER.

Vous devez avoir reçu une copie de la GNU General Public License en même temps que Sésathèque
(fichier LICENSE)

Plus d'infos en français sur http://vvlibri.org/fr/Analyse/gnu-affero-general-public-license-v3-analyse
avec également (en anglais) http://www.gnu.org/licenses/agpl.txt
et https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)
