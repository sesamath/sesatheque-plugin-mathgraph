'use strict'
import dom from 'sesajstools/dom'
import log from 'sesajstools/utils/log'

// on est compilé par le webpack de la sesatheque qui sait où c'est
import page from 'client/page'
import serverConfig from 'server/config'

const { addElement } = dom
const { application: { staging } } = serverConfig

// on veut prod|production mais pas -pre-prod- ni preprod
const isProd = /^prod/.test(staging)

// pour tester du dev local de mathgraph, lancé par `pnpm run start`, mettre
// const mtgLoadUrl = 'http://localhost:8080/devServer/mtgLoad.js'
const mtgLoadUrl = isProd
  ? 'https://www.mathgraph32.org/js/mtgLoad/mtgLoad.min.js'
  : 'https://dev.mathgraph32.org/js/mtgLoad/mtgLoad.min.js'

/**
 * Affiche une ressource mathgraph, avec l'applet java ou le lecteur js (suivant paramétrage de la ressource)
 * On peut forcer le js en précisant ?js=1 dans l'url
 * @service plugins/mathgraph/display
 * @param {Ressource}      ressource  L'objet ressource (une ressource mathgraph a en parametres soit une propriété url
 *                                      avec l'url du xml soit une propriété xml avec la string xml)
 * @param {displayOptions} options    Les options après init
 * @param {errorCallback}  next       La fct à appeler quand l'mathgraph sera chargé (sans argument ou avec une erreur)
 */
export function display (ressource, options, next) {
  // on ne vérifie que la figure et le score
  function isSameResultat (resultat) {
    if (!resultat) throw new Error('Erreur interne')
    if (!lastResultatSent) return false
    return resultat.score === lastResultatSent.score &&
      resultat.contenu && lastResultatSent.contenu &&
      resultat.contenu.fig === lastResultatSent.contenu.fig
  }

  // si y'a pas de cb on affiche les erreurs sur la page (et en console)
  if (!next) {
    next = (error) => {
      if (error) page.addError(error)
    }
  }
  // une fct pour n'appeler next qu'une seule fois
  const callNextOnce = (error, app) => {
    if (isNextCalled) return log.error(error || Error('2e appel de next sans argument'))
    isNextCalled = true
    next(error)
  }

  let isLoaded = false
  let isNextCalled = false
  let lastResultatSent
  const { container, resultatCallback, verbose } = options
  log.setLogLevel(verbose ? 'debug' : 'warning')

  try {
    if (!container) throw new Error('Il faut passer dans les options un conteneur html pour afficher cette ressource')

    log('start mathgraph display avec la ressource', ressource)
    // les params minimaux
    if (!ressource.oid || !ressource.titre || !ressource.parametres) throw new Error('Ressource incomplète')
    const { parametres } = ressource
    const { content, isEditable, isInteractive } = parametres
    if (!content || !content.fig) throw new Error('Pas de figure mathgraph en paramètre')

    page.loadAsync([mtgLoadUrl], function () {
      try {
        if (typeof mtgLoad !== 'function') throw new Error('Mathgraph n’est pas chargé')
        /* global mtgLoad */
        const svgOptions = {}
        if (Number.isInteger(parametres.width)) svgOptions.width = parametres.width
        if (Number.isInteger(parametres.height)) svgOptions.height = parametres.height

        // la consigne éventuelle (disparue dans les nouvelles versions de mathgraph, à mettre dans la figure)
        if (parametres.consigne) addElement(container, 'p', null, parametres.consigne)

        // on va lui ajouter des props => shallowCopy pour éviter de muter parametres.content
        // en ajoutant des props à mtgOptions
        const mtgOptions = Object.assign({}, content, { isEditable, isInteractive })
        // ces deux paramètres sont valables pour le player et l'éditeur
        mtgOptions.decimalDot = Boolean(parametres.decimalDot)
        mtgOptions.useLens = Boolean(parametres.useLens)
        // les autres pour l'éditeur seulement
        if (isEditable) {
          if (typeof mtgOptions.level !== 'number') mtgOptions.level = 1
          // en consultation on ne peut pas remplacer la figure par une nouvelle, sauf si c'est explicitement autorisé
          mtgOptions.newFig = Boolean(parametres.newFig)
          // idem pour en ouvrir une
          mtgOptions.open = Boolean(parametres.open)
          // options: pour autoriser à changer les options de la figure, true par défaut, jamais autorisé en consultation
          mtgOptions.options = false

          // en consultation (preview), le bouton save doit être affiché si c'est un exo de construction
          // ou si y'a une cb. Vu qu'on ne sait pas ici si c'est un exo de construction ou pas, on passe
          // toujours true, mais ce param devrait disparaître (et mtg décide s'il met le bouton ou pas)
          mtgOptions.save = true

          // sert à distinguer  dans mathgraph le mode affichage / édition de la bibli, utile pour recalculer de l'aléatoire dans les figures de construction
          // (preview false c'est une modif, c'est le prof qui crée l'exo de construction, preview true c'est l'élève qui travaille)
          mtgOptions.preview = true

          // pour activer les points en croix (éditeur seulement)
          mtgOptions.stylePointCroix = Boolean(parametres.stylePointCroix)
        }

        // cb de fin de chargement (attention, le 1er argument est l'appli)
        mtgOptions.callBackAfterReady = () => callNextOnce()

        // on ajoute une cb si qq veut le résultat
        if (resultatCallback) {
          if (isEditable) {
            // sauvegarde la figure courante
            const save = (needDefer) => {
              // on veut dys et level pour le display du bilan, mais on laisse getResult l'écraser s'il le souhaite
              const contenu = mtgApp.getResult()
              // on ajoute ça car la bibli va remplacer les scores undefined par 0 avant de les envoyer
              // (pour garantir un nombre entre 0 et 1) et on veut distinguer
              // les score 0 (exos de construction ratés)
              // des score undefined (figures sans score)
              contenu.isScored = typeof contenu.score === 'number'
              const resultat = {
                contenu,
                score: contenu.score,
                fin: true
              }
              if (!isSameResultat(resultat)) {
                if (needDefer) resultat.deferSync = true
                resultatCallback(resultat)
                lastResultatSent = resultat
              }
            } // save

            // cb sur le bouton save
            mtgOptions.functionOnSave = save.bind(null, false)
            // + listener unload
            container.addEventListener('unload', () => {
              if (isLoaded) save(true)
            })
            // + notif
            page.showNotification('<p style="max-width: 300px">Clique sur le bouton de sauvegarde <img src="/plugins/mathgraph/outilSave.png" /> pour enregistrer ton résultat (cela terminera l’exercice)</p>')
          } else {
            // player only, on ajoute le bouton vu
            let isResultatSend = false
            const resultat = {
              fin: true,
              score: 1,
              reponse: 'Vu'
            }
            const sendResultatVu = () => {
              if (isResultatSend) return
              isResultatSend = true
              resultatCallback(resultat)
            }
            page.addBoutonVu(sendResultatVu)
            // et le listener unload
            container.addEventListener('unload', sendResultatVu)
          }
        } // fin

        // go
        let mtgApp
        log.debug('appel mtgLoad avec les options', { svgOptions, mtgOptions })
        mtgLoad(container, svgOptions, mtgOptions, function (error, app) {
          if (error) return callNextOnce(error)
          // chargement ok, c'est callBackAfterReady qui appelera next
          mtgApp = app
          isLoaded = true
        })
      } catch (error) {
        callNextOnce(error)
      }
    })
  } catch (error) {
    callNextOnce(error)
  }
}
