Plugin Mathgraph pour l'application Sésathèque
==============================================

Installation
------------

Pour être inclus dans une sésathèque, elle doit préciser dans son `_private/config.js`

```
plugins: {
  external: {
    '@sesatheque-plugins/mathgraph': true 
    // ou 'git+https://forge.apps.education.fr/sesamath/sesatheque-plugin-mathgraph.git#x.y.z'
    // pour préciser une version particulière
  }
}
```

Sur cette sésathèque
* `pnpm install` ajoute ou met à jour le plugin dans `app/plugins/node_modules/@sesatheque-plugins/mathgraph`
* `pnpm build` compile les js du plugin (son webpack.config.js utilise app/plugins/webpack.config.js qui récupère les props `{entries, plugins, rules}` retournées par chaque `{plugin}/webpack.config.js`

Le build copie aussi le contenu de assets/ dans sesatheque:build/plugins/mathgraph/ (cf notre webpack.config.js), ils seront donc accessible en http sur le domaine de la sésathèque avec l'url /plugins/mathgraph/fichier.ext

Dépendances
-----------

La configuration webpack de la Sésathèque nous permet de faire des require de ses fichiers (relativement à son dossier app/ ou à ses node_modules). 

On utilise : 
* sesatheque
  * server/config
  * client/page/index
  * client-react
* modules externes (fournies par sesatheque)
  * sesajstools
  * prop-types
  * react
  * redux-form

Dev local
---------

`build:watch` fonctionne avec les js du plugin, mais pour éditer les fichiers du dépôt local du plugin et les voir recompiler à la volée, il faut
* dans le dépôt local de la sesathèque faire un `cd app/plugins && pnpm link path/to/sesatheque-plugins-mathgraph && cd ../..`
* (unlink pour retirer cette liaison)

Ça fonctionne avec par ex la structure
* workingDir
* workingDir/sesatheque (dépôt git)
* workingDir/sesatheque-plugin-mathgraph (dépôt git)
* workingDir/node_modules -> sesatheque/node_modules
