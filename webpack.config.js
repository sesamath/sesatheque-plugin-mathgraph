const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')

/**
 * Éléments de configuration webpack qui seront mergés avec ceux de la sésathèque
 * @typedef pluginWebpackConfig
 * @type {Object}
 * @property {Object} entries
 * @property {Object[]} plugins
 * @property {Object[]} rules (mergé dans sesathequeWebpackConfig.modules.rules)
 */
/**
 * chargé par app/plugins/webpack.config.js, qui nous appelle avec ({version})
 * @param {Object} config
 * @param {string} config.version
 * @return {pluginWebpackConfig}
 */
module.exports = ({ version }) => ({
  entries: {
    // attention au nom de l'entry, qui conditionne le nom de la fonction ajoutée en global dans le dom
    // (préfixe "st" ajouté par le webpack de la sesatheque) qui est utilisée par mathgraph-editor.html
    pluginMathgraphEditor: path.resolve(__dirname, 'editor', 'mathgraph-editor.js')
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: path.resolve(__dirname, 'assets', '*'),
        // pour pointer sur build/plugins/mathgraph/fichier.ext et pas build/plugins/sesatheque-plugin-mathgraph/assets/fichier.ext
        to: 'plugins/mathgraph/[name][ext]' // surtout pas de [contenthash], faut préserver les noms de fichier
      }, {
        from: path.resolve(__dirname, 'editor', 'mathgraph-editor.html'),
        to: 'plugins/mathgraph/[name][ext]',
        transform: (content) => content.toString().replace(/{version}/g, version)
      }]
    })
  ],
  rules: []
})
