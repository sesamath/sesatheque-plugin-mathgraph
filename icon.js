import icon from './assets/mathgraph.gif'
import type from './type'

export { type, icon }
