// Ce js est utilisé pour charger mathgraph quand on édite une ressource.
//
// Il est compilé par webpack (cf ../webpack.entry.js) et mis dans /pluginMathgraphEditor.js
// pour être chargé dans mathgraph-editor.html, iframe de la page d'édition (qui contient
// editor/EditorMathGraph.js pour gérer le reste du form, le chargement et la sauvegarde)

// Quand le plugin est installé ce js se retrouve copié dans
// app/plugins/node_modules/@sesatheque-plugin/mathgraph/editor/mathgraph-editor.js
// mais la config de webpack de la sésathèque permet de faire des require en relatif
// à son dossier app/

import sjt from 'sesajstools'
import dom from 'sesajstools/dom'
import serverConfig from 'server/config'

const { hasProp } = sjt
const { application: { staging } } = serverConfig

const { addElement, addJs } = dom

// idem display/index.js
const isProd = /^prod/.test(staging)

const mtgLoadUrl = isProd
  ? 'https://www.mathgraph32.org/js/mtgLoad/mtgLoad.min.js'
  : 'https://dev.mathgraph32.org/js/mtgLoad/mtgLoad.min.js'

export default function addMtgLoader ({ parametres }, input) {
  function displayEditor () {
    /* global mtgLoad */

    // hauteur et largeur toujours d'après la taille de l'écran en édition
    // (les paramètres c'est pour la figure élève)
    // faut retirer 16 pour éviter la scrollbar
    let width = parametres.width || (document.body.clientWidth - 16)
    if (!Number.isInteger(width) || width < 300) width = 300
    // qu'on met dans svgOptions
    const svgOptions = {
      width,
      height: 760 // min-height à 800 imposé par le css sur l'iframe, faut retirer 40 pour éviter la scrollbar
    }

    const mtgOptions = parametres.content || {}

    // à la création on démarre avec une interface collège
    // (le prof pourra changer ensuite dans l'éditeur
    // et getParametres nous renverra la valeur modifiée)
    // le level sert pour ouvrir mathgraph avec une figure vierge, ou pour limiter les outils disponibles
    // il peut être mdifié dans l'interface mtg si mtgOptions.options === true
    // 0 : école
    // 1 : collège
    // 2 : lycée sans les nombres complexes
    // 3 : lycée avec les nombres complexes
    // Si la figure est fournie, en édition il faut fournir le level le plus élevé
    // pour que le prof puisse activer tous les outils qu'il voudrait ajouter
    if (!hasProp(mtgOptions, 'level')) mtgOptions.level = 1
    // à la création on démarre avec ça par défaut (sans repère, juste vecteur unité)
    if (!mtgOptions.fig) mtgOptions.fig = { type: 'unity', grad: 'simple', unity: 'deg' }
    // si c'est pas précisé on utilise la virgule comme séparateur décimal
    mtgOptions.decimalDot = Boolean(parametres.decimalDot)
    // ce paramètre dys à true sert à afficher la figure avec des traits plus gros,
    // des lettres plus espacées, etc.
    // Il n'est pas contenu dans la figure, et getResult retourne la valeur avec laquelle l'éditeur a été chargé (pas le param courant)
    mtgOptions.dys = Boolean(parametres.dys)
    mtgOptions.stylePointCroix = Boolean(parametres.stylePointCroix)
    mtgOptions.useLens = Boolean(parametres.useLens)
    // pour les exercices de construction c'est indispensable, le prof doit pouvoir tout éditer
    // (sinon le prof a la même chose que l'élève qui fait un exo de construction
    // et qui ne peut pas modifier les objets initiaux)
    mtgOptions.editionConstruction = true
    // bouton pour ouvrir une figure, l'info est dans la figure mais on le force ici (édition)
    mtgOptions.open = true
    // idem pour nouvelle figure
    mtgOptions.newFig = true
    // options: pour autoriser à changer les options de la figure, true par défaut, toujours true en édition
    mtgOptions.options = true
    // on vire le bouton pour éviter la confusion avec le bouton sauvegarde de la ressource sous la figure
    // si le prof veut récupérer sa figure il peut toujours l'exporter en base64
    mtgOptions.save = false
    // onSaveCallback n'a de sens que si `figureOptions.save === true`,
    // ici on laisse le comportement par défaut sur le bouton enregistrement de mathgraph
    // (en fichier local et pas dans la sesatheque) car on récupère la figure au clic sur
    // enregistrer (dans la page html hors mathgraph)

    // quand mtg sera chargé on mettra nos listeners
    mtgOptions.callBackAfterReady = function (mtgApp) {
      // on vire le spinner
      const spinner = document.getElementById('spinner') || {}
      while (spinner.firstChild) spinner.removeChild(spinner.firstChild)
      // et on ajoute nos listeners pour passer les infos à redux-form
      window.addEventListener('blur', function () {
        const content = mtgApp.getResult()
        if (!content || typeof content !== 'object') throw new Error('mathgraph ne remonte aucune info')
        input.onBlur(content)
      })
      window.addEventListener('click', function () {
        window.focus()
      })
      window.addEventListener('focus', function () {
        input.onFocus()
      })
    } // callBackAfterReady

    const loadingPromise = mtgLoad('main', svgOptions, mtgOptions)
    if (loadingPromise instanceof Promise) loadingPromise.catch(showError)
    else console.error(Error('mtgLoad appelé sans callback ne retourne pas une promesse'))
  } // displayEditor

  if (typeof window === 'undefined') throw Error('Ce code ne peut s’exécuter que dans un navigateur')
  let errorsContainer = document.getElementById('errors')
  if (!errorsContainer) errorsContainer = addElement(document.body, 'div', { id: 'errors' })
  const showError = (error) => {
    console.error(error)
    const errorMessage = error.message || error
    errorsContainer.innerText = errorMessage
  }
  if (typeof mtgLoad === 'function') {
    displayEditor()
  } else {
    addJs(mtgLoadUrl, () => {
      if (typeof mtgLoad === 'function') displayEditor()
      else showError(Error('Mathgraph n’est pas chargé correctement'))
    })
  }
}
