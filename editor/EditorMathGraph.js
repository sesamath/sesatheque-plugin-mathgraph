import PropTypes from 'prop-types'
import React, { Fragment, Component } from 'react'
import { formValues } from 'redux-form'
import { IframeField, IntegerField, SwitchField } from 'client-react/components/fields'
import sjt from 'sesajstools'

const { hasProp } = sjt

// page de l'éditeur mathgraph à insérer en iframe (copiée par webpack)
const iframeSrc = '/plugins/mathgraph/mathgraph-editor.html'

class EditorMathGraph extends Component {
  constructor (props) {
    super(props)
    // un compteur pour obliger l'iframe à se recharger (si on fait un setState dans
    // componentDidUpdate ça relance un render, qui va recharger l'iframe car son url aura changé)
    this.state = {
      i: 1
    }
    // defaultProps ne fonctionne que pour les props de la racine, on veut du nested
    if (!hasProp(props.parametres, 'isEditable')) props.parametres.isEditable = true
    if (!hasProp(props.parametres, 'isInteractive')) props.parametres.isInteractive = true
  }

  componentDidUpdate (prevProps) {
    // on va recharger l'iframe (via le setState qui incrémente i et relance un rendu) si ces props changent
    const paramProps = ['dys', 'stylePointCroix', 'decimalDot', 'useLens']
    if (!paramProps.every(p => this.props.parametres[p] === prevProps.parametres[p])) {
      // un param a changé, faut récupérer la figure courante et recharger
      this.setState({
        i: this.state.i + 1
      })
    }
  }

  /**
   * Appelée par le onLoad de l'iframe
   * @param {HTMLElement} iframe Iframe présente dans le DOM
   * @param input
   */
  onIframeLoaded (iframe, input) {
    const { parametres } = this.props
    const win = iframe.current.contentWindow
    // load correspond à la fct exportée par mathgraph-editor.js
    if (typeof win.load !== 'function') throw Error('win.load KO')
    win.load({ parametres }, input)
  }

  render () {
    const isFigEmpty = !this.props.parametres.content || this.props.parametres.content.fig === undefined
    const isEditable = this.props.parametres.isEditable

    return (
      <Fragment>
        <fieldset>
          <div className="grid-3">
            <IntegerField
              label="Largeur imposée"
              info="à l’élève, laisser vide pour s'adapter à son écran"
              name="parametres[width]"
              min="300"
            />
            <IntegerField
              label="Hauteur imposée"
              info="à l’élève, laisser vide pour s'adapter à son écran"
              name="parametres[height]"
              min="200"
            />
            <SwitchField
              label="Éditable (affichage avec boutons et menus)"
              name="parametres[isEditable]"
            />
          </div>
          <div className="grid-4">
            {isEditable
              ? (
              <Fragment>
                <SwitchField
                  label="Afficher à l'élève le bouton «&nbsp;nouvelle figure&nbsp;»"
                  name="parametres[newFig]"
                />
                <SwitchField
                  label="Afficher à l'élève le bouton «&nbsp;ouvrir&nbsp;»"
                  name="parametres[open]"
                />
                <SwitchField
                  label="Sélectionner le style de point «&nbsp;×&nbsp;» (grand)"
                  name="parametres[stylePointCroix]"
                />
                <SwitchField
                  label="Affichage adapté «&nbsp;dys&nbsp;»"
                  name="parametres[dys]"
                />
              </Fragment>
                )
              : (<SwitchField
                  label="Figure interactive (points mobiles et boutons éventuels)"
                  name="parametres[isInteractive]"
                />)
            }
            <SwitchField
              label="Point comme séparateur décimal"
              name="parametres[decimalDot]"
            />
            <SwitchField
              label="Activer la loupe"
              name="parametres[useLens]"
            />
          </div>
        </fieldset>
        <hr />
        <IframeField
          label="Édition du contenu MathGraph"
          name="parametres[content]"
          onLoad={this.onIframeLoaded.bind(this)}
          src={`${iframeSrc}?${this.state.i}`}
        >
          {isFigEmpty
            ? (
            <div className="alert--info">Pour ajouter un repère, utiliser le bouton
            «&nbsp;Nouvelle figure&nbsp;»</div>
              )
            : null}
          <div className="alert--info">Vous pouvez changer les outils disponibles via le bouton
            «&nbsp;options&nbsp;» <img src="/plugins/mathgraph/outilOptionsFig.png"/></div>
        </IframeField>
      </Fragment>
    )
  }
}

EditorMathGraph.propTypes = {
  parametres: PropTypes.object.isRequired
}

export default formValues({ parametres: 'parametres' })(EditorMathGraph)
